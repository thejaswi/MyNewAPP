import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './views/home.component'
import { JavaComponent } from './views/java.component';
import { AndroidComponent } from './views/android.component';
import { routes } from './app.route';
import { EmployeeService } from './services/employee.service';
import { CountriesService } from './services/countries.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    JavaComponent,
    AndroidComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule

  ],
  providers: [ EmployeeService,CountriesService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
