import { Component , OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { NgModule } from '@angular/core';
import { NgSwitch } from '@angular/common';
import { RouterModule } from '@angular/router';
import {  EmployeeService } from './services/employee.service';
import { CountriesService } from './services/countries.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ EmployeeService , CountriesService ]
})
export class AppComponent implements OnInit {
  public title = 'Angular 4';
  

  public myStyle = {
    'color': 'blue',
    'background-color':'yellow'
  };

  public showText = false;

  public switchOff(){
    this.showText = !this.showText;
  };

  public textColor = 'GREEN';

  public players =[
    { name:'Dravid' , city:'BLore'},
    { name:'MSD' , city:'Ranchi'},
    { name:'Sachin' , city:'Mumbai'}
  ];
 
  public employeeObj : any ;
  public empId : string;

  constructor( private employeeService : EmployeeService , private countriesService : CountriesService ){

  }

  public getEmployeeDetails(): void {
    this.employeeObj = this.employeeService.getEmplooyee( this.empId );
  }


  public countryObj : any ;
  public countryName : string ;
  public countriesList : any ;

 
  public getCountryInfo(){
    this.countriesService.getCountryDetails( this.countryName ).subscribe( res => this.countryObj = res.json()[0] )
  }

  ngOnInit(){
    this.countriesService.getAllCountries().subscribe( res => this.countriesList = res.json() )
  }

}
