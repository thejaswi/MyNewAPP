  import { Injectable } from '@angular/core';

  @Injectable()
  export class EmployeeService {

  public employeeRecord =[
      {name:'sachin' , city:'Mumbai' , age : '45' , id : 'e1'},
      {name:'Dravid' , city:'Bangalore' , age : '54' , id : 'e2'},
      {name:'Dhoni' , city:'Ranchi' , age : '40' , id : 'e3'},
      {name:'Yusaf' , city:'Varodra' , age : '50' , id : 'e4'}
  ];

        getEmplooyee ( id : string) : any {
          let employee : any;
          for(let i = 0; i < this.employeeRecord.length; i++){
            if( id === this.employeeRecord[i].id){
              employee = this.employeeRecord[i];
              break;
            }
          }
          return employee;
        }
        
}
